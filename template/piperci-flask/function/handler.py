"""
This module is for testing and validation only
    and does not represent a real use of a handler for a PiperCI Task
"""
from flask import Request

from piperci.task.exceptions import PiperDelegateError, PiperError
from piperci.task.this_task import ThisTask

from .models import task_schema_gen


def gateway(request: Request, task: ThisTask, config: dict):
    """Handle gateway delegation requests.
    :request: Flask request object
    :task: PiperCI ThisTask object
    :config: Compiled piperci_flask.config object

    This module is for testing and validation only
        and does not represent a real use of a handler for a PiperCI Task
    """
    try:
        task.info("task gateway handler.handle called successfully")
    except PiperError as e:
        return {"errors": {"handler:gateway" [
            f"{str(e)}: no delegate was attempted"]}}, 400

    try:
        schema = task_schema_gen()
        task.delegate(config["executor_url"], data=schema().dump(config["task_config"]))
        return task.complete("Basic test gateway completed successfully")
    except PiperDelegateError as e:
        return task.fail(str(e))


def executor(request: Request, task: ThisTask, config: dict):
    """Handle executor requests.
    :request: Flask request object
    :task: PiperCI ThisTask object
    :config: Compiled piperci_flask.config object

    This module is for testing and validation only
        and does not represent a real use of a handler for a PiperCI Task
    """
    result = task.complete("Test stub of executor")
    result[0]["config"] = config
    return result
