"""This module is for testing and validation only
and does not represent a real use of a config for a PiperCI Task
"""

Config = {
    "gman": {"url": "http://gman:8080"},
    "storage": {
        "url": "minio:9000",
        "access_key_secret": "/var/openfaas/secrets/access-key",
        "secret_key_secret": "/var/openfaas/secrets/secret-key",
    },
    "name": "faas_template_test_handler",
    "executor_url": "http://gateway:8080/async-function/piperci-flask/executor",
    "custom_schema_module": "",
    "custom_schema_class": "",
}

# No coverage due to difficulty mocking logic executed during an import
try:  # pragma: no cover
    with open(Config["storage"]["access_key_secret"], "r") as access_key_file:
        Config["storage"]["access_key"] = access_key_file.readline().strip("\n")

    with open(Config["storage"]["secret_key_secret"], "r") as access_key_file:
        Config["storage"]["secret_key"] = access_key_file.readline().strip("\n")
except (KeyError, IOError):
    Config["storage"]["access_key"] = "test"
    Config["storage"]["secret_key"] = "test_secret"
