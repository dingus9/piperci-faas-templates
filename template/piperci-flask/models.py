from piperci.gman import client as gman_client
from piperci.sri import sri_to_hash
import requests.exceptions
from marshmallow import fields, Schema, ValidationError, EXCLUDE, INCLUDE
from .config import Config
import importlib


def validate_task_id(task_id):
    try:
        if not gman_client.get_task(gman_url=Config["gman"]["url"], task_id=task_id):
            raise ValidationError(f"Task ID {task_id} does not exist")
    except requests.exceptions.RequestException:
        raise ValidationError(f"Unable to contact GMan to validate TaskID")


def validate_run_id(run_id):
    try:
        if not gman_client.get_run(gman_url=Config["gman"]["url"], run_id=f"{run_id}"):
            raise ValidationError(f"Run ID {run_id} does not exist")
    except requests.exceptions.RequestException:
        raise ValidationError(f"Unable to contact GMan to validate RunID")


def validate_thread_id(thread_id):
    try:
        if not gman_client.get_thread(
            gman_url=Config["gman"]["url"], thread_id=thread_id
        ):
            raise ValidationError(f"Thread ID {thread_id} does not exist")
    except requests.exceptions.RequestException:
        raise ValidationError(f"Unable to contact GMan to validate ThreadID")


def validate_sri(sri):
    try:
        sri_to_hash(sri)
    except Exception:
        raise ValidationError("Not a valid SRI")


def task_schema_gen():
    """Runtime generator for TaskSchema due to some testing requirements"""
    if Config.get("custom_schema_module"):
        CustomSchema = getattr(
            importlib.import_module(
                f".{Config.get('custom_schema_module')}",
                package=__package__
            ),
            Config.get("custom_schema_class"),
        )
    else:
        class CustomSchema(Schema):
            class Meta:
                unknown = INCLUDE

    class TaskSchema(Schema):
        class Meta:
            unknown = EXCLUDE

        project = fields.Str(required=True)
        config = fields.Nested(CustomSchema, required=True)
        stage = fields.Str(required=True)
        thread_id = fields.UUID(required=False,
                                default=None,
                                missing=None,
                                validate=validate_thread_id)
        parent_id = fields.UUID(required=True,
                                validate=validate_task_id)
        run_id = fields.Str(required=True, validate=validate_run_id)
        only_validate = fields.Boolean(required=False,
                                       default=False,
                                       missing=None,)

        artifacts = fields.List(fields.Str(validate=validate_sri),
                                required=False,
                                missing=[],
                                default=[])

    return TaskSchema
