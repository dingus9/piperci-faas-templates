# PiperCI FaaS Templates

OpenFaaS templates for PiperCI FaaS functions

## Provided Templates

### `piperci-flask`

A flask template based off of the [python3-flask: ccf3ba6](https://github.com/openfaas-incubator/python-flask-template/tree/ccf3ba6/template/python3-flask) template.

- [PiperCI-Flask Docs](https://piperci.dreamer-labs.net/faas-templates/README.html)
- **Example Function**: [PiperCI-Echo-FaaS](https://piperci.dreamer-labs.net/echo-faas/README.html)

### Quick usage

All paths in this document assume / to be the root of the git repository code is referenced in.

1. Install these base templates in the functions base directory
1. Define the function in the /stack.yml
1. Develop the function
1. Add system packages to system-packages.txt in the /{function_name}/system-packages.txt
1. Add python requirements to /{function_name}/requirements.txt
1. Build the function against the base template from this repository

### Installation and building

#### Install base templates

`faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates`

#### Develop function

Several general function patterns have been provided in all templates to ease the deployment process

1. Define docker system level packages in /{function_name}/system-packages.txt. These should be the appropriate package name for the target platform.
1. For python langs add the requirements to /{function_name}/requirements.txt
1. Define the function's stack.yml

#### Build function

```
faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates
faas-cli build
```
